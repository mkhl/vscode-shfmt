import vscode from 'vscode'

const root = 'shfmt'

function config(): vscode.WorkspaceConfiguration {
	return vscode.workspace.getConfiguration(root)
}

class Setting<T> {
	constructor(private path: string, private defaultValue: T) {}

	get(): T {
		return config().get(this.path) ?? this.defaultValue
	}

	async open() {
		return await vscode.commands.executeCommand(
			'workbench.action.openSettings',
			`${root}.${this.path}`,
		)
	}
}

type Settings<Type> = {
	[Name in keyof Type]: Setting<Type[Name]>
}

function convert<Type extends object>(defaults: Type): Settings<Type> {
	return Object.fromEntries(
		Object.entries(defaults).map(([key, value]) => [key, new Setting(key, value)]),
	) as Settings<Type>
}

export default convert({
	executablePath: 'shfmt',
	executableArgs: [] as string[],
	formatIgnored: false,
})
