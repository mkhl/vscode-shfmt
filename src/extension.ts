import cp from 'child_process'
import vscode from 'vscode'
import os from 'os'
import { Position, Range, TextEdit } from 'vscode'
import config from './config'
import * as editorconfig from './editorconfig'

const everything = new Range(0, 0, Number.MAX_SAFE_INTEGER, Number.MAX_SAFE_INTEGER)
const errors = /^.+:(\d+):(\d+): (.+)/g
const installationUri = vscode.Uri.parse('https://github.com/mvdan/sh#shfmt')

class Formatter {
	constructor(private diagnostics: vscode.DiagnosticCollection) {}

	resolveVariables(values: string[], document: vscode.TextDocument): string[] {
		const substitutions = new Map<string, string>()
		const home = os.homedir() || process.env.USERPROFILE
		if (home) {
			substitutions.set('${userHome}', home)
		}
		const uri = document.uri
		if (uri) {
			const workspaceFolder = vscode.workspace.getWorkspaceFolder(uri)
			if (workspaceFolder) {
				substitutions.set('${workspaceFolder}', workspaceFolder.uri.fsPath)
			}
		}
		vscode.workspace.workspaceFolders?.forEach((w) => {
			substitutions.set(`\${workspaceFolder:${w.name}}`, w.uri.fsPath)
		})
		return values.map((s) => {
			for (const [key, value] of substitutions) {
				s = s.replace(key, value)
			}
			return s
		})
	}

	async provideDocumentFormattingEdits(
		document: vscode.TextDocument,
		options: vscode.FormattingOptions,
		cancel: vscode.CancellationToken,
	): Promise<TextEdit[] | undefined> {
		const args = await this.shfmtArgs(document, options)
		if (args === null) return
		const cwd = vscode.workspace.workspaceFolders?.[0].uri.fsPath ?? process.cwd()
		const [configPath, ...configArgs] = this.resolveVariables(
			[config.executablePath.get(), ...config.executableArgs.get()],
			document,
		)
		const shfmt = cp.spawn(configPath, [...configArgs, ...args], { cwd })
		cancel.onCancellationRequested(() => shfmt.kill())
		const total = document.validateRange(everything)
		const stdin = document.getText(total)
		shfmt.stdin.end(stdin)
		let stdout = ''
		let stderr = ''
		shfmt.stdout.on('data', (chunk) => (stdout += chunk))
		shfmt.stderr.on('data', (chunk) => (stderr += chunk))
		try {
			const code = await new Promise<number | null>((c, e) => {
				shfmt.on('error', (err) => e(err))
				shfmt.on('close', (code) => c(code))
			})
			if (cancel.isCancellationRequested) return
			this.diagnostics.delete(document.uri)
			if (code === 0) return [TextEdit.replace(total, stdout)]
			const problems = [...stderr.matchAll(errors)].map(([_, line, column, msg]) => {
				const point = new Position(parseInt(line) - 1, parseInt(column) - 1)
				return new vscode.Diagnostic(new Range(point, point), msg)
			})
			this.diagnostics.set(document.uri, problems)
			throw stderr
		} catch (err) {
			if (isCommandNotFound(err, configPath)) {
				await this.shfmtNotFound(configPath)
				return
			}
			const msg = message(err)
			if (msg !== undefined) {
				await vscode.window.showErrorMessage(`shfmt error: ${msg}`)
			}
			return
		}
	}

	private async shfmtArgs(
		document: vscode.TextDocument,
		options: vscode.FormattingOptions,
	): Promise<string[] | null> {
		const resolved = await editorconfig.resolve(document)
		if (resolved.ignore && !config.formatIgnored.get()) return null
		const args = []
		if (!document.isUntitled) {
			args.push(`-filename=${document.uri.fsPath}`)
		}
		if (!('indent_style' in resolved || 'indent_size' in resolved)) {
			args.push(`-i=${options.insertSpaces ? options.tabSize : 0}`)
		}
		return args
	}

	private async shfmtNotFound(path: string) {
		const options = ['Install', 'Configure']
		const choice = await vscode.window.showErrorMessage(
			`shfmt error: ${path}: command not found`,
			...options,
		)
		if (choice === 'Install') {
			await vscode.env.openExternal(installationUri)
		}
		if (choice === 'Configure') {
			await config.executablePath.open()
		}
	}
}

function isCommandNotFound(e: unknown, path: string): boolean {
	if (!(e instanceof Error)) return false
	if (!('path' in e) || !('code' in e)) return false
	return e['path'] === path && e['code'] === 'ENOENT'
}

function message(err: unknown): string | undefined {
	if (typeof err === 'string') {
		return err
	}
	if (err instanceof Error) {
		return err.message
	}
	console.error('unhandled error', err)
	return undefined
}

export function activate(context: vscode.ExtensionContext) {
	const diagnostics = vscode.languages.createDiagnosticCollection('shfmt')
	const formatter = new Formatter(diagnostics)
	const selector = { language: 'shellscript' }
	context.subscriptions.push(
		diagnostics,
		vscode.languages.registerDocumentFormattingEditProvider(selector, formatter),
	)
}
